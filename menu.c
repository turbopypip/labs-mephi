#include <stdio.h>
#include <stdlib.h>
#include "Matrix.h"
#include "MatrixElement.h"
#include "LoggingFlags.h"
#include "menu.h"

void printMenu() {
    printf("\nMenu\n");
    printf("0. Exit\n");
    printf("1. Create matrix\n");
    printf("2. Delete matrix by index\n");
    printf("3. Sum matrices\n");
    printf("4. Multiply matrices\n");
    printf("5. Add linear combination\n");
    printf("6. Multiply matrix by number\n");
    printf("7. Print all matrices\n");
}

void menu(){
    int countMatrices = 0;
    Matrix** arrayOfMatrices = NULL;
    bool running = true;
    int choice;
    while (running) {
        printMenu();
        scanf("%d", &choice);
        switch (choice) {
            case 0: {
                running = false;
                break;
            }
            case 1: {
                countMatrices++;
                arrayOfMatrices = (Matrix**)realloc(arrayOfMatrices, countMatrices * sizeof(Matrix));
                if (arrayOfMatrices == NULL) {
                    break;
                }
                int size;
                printf("Enter size of matrix: ");
                scanf("%d", &size);
                while (size < 1) {
                    printf("\nEnter size bigger than 0: ");
                    scanf("%d", &size);
                }
                printf("\n");

                int number_type_choice;
                printf("Enter number type:\n 1 - int,\n 2 - float,\n 3 - double,\n 4 - long\n");
                scanf("%d", &number_type_choice);
                while (number_type_choice < 1 || number_type_choice > 4) {
                    printf("Enter valid number type:\n 1 - int,\n 2 - float,\n 3 - double,\n 4 - long\n");
                    scanf("%d", &number_type_choice);
                }

                switch (number_type_choice) {
                    case 1: {
                        arrayOfMatrices[countMatrices - 1] = createMatrix(size, sizeof(int));
                        printf("Enter matrix elements:\n");
                        int ***elements = malloc(2 * sizeof(int **));
                        for (int i = 0; i < size; ++i) {
                            elements[i] = malloc(size * sizeof(int *));
                            for (int j = 0; j < size; ++j) {
                                int *e = malloc(sizeof(int));
                                scanf("%d", e);
                                elements[i][j] = e;
                            }
                        }

                        int res = addElements(arrayOfMatrices[countMatrices - 1],
                                              (void ***) elements,
                                              sizeof(int),
                                              &IntField);
                        if (res != 0) {
                            if (res == MATRIX_NULL) {
                                printf("Matrix %d is null\n", countMatrices - 1);
                            } else if (res == FAILED_MALLOC) {
                                printf("Failed to allocate memory\n");
                            } else if (res == ELEMENTS_ARRAY_NULL) {
                                printf("Elements array is null\n");
                            }
                            break;
                        } else {
                            printf("Matrix %d created successfully\n", countMatrices);
                        }

                        break;
                    }
                    case 2: {
                        arrayOfMatrices[countMatrices - 1] = createMatrix(size, sizeof(float));
                        printf("Enter matrix elements:\n");
                        float ***elements = malloc(2 * sizeof(float **));
                        for (int i = 0; i < size; ++i) {
                            elements[i] = malloc(size * sizeof(float *));
                            for (int j = 0; j < size; ++j) {
                                float *e = malloc(sizeof(float));
                                scanf("%f", e);
                                elements[i][j] = e;
                            }
                        }

                        int res = addElements(arrayOfMatrices[countMatrices - 1],
                                              (void ***) elements,
                                              sizeof(float),
                                              &FloatField);
                        if (res != 0) {
                            if (res == MATRIX_NULL) {
                                printf("Matrix %d is null\n", countMatrices - 1);
                            } else if (res == FAILED_MALLOC) {
                                printf("Failed to allocate memory\n");
                            } else if (res == ELEMENTS_ARRAY_NULL) {
                                printf("Elements array is null\n");
                            }
                            break;
                        } else {
                            printf("Matrix %d created successfully\n", countMatrices);
                        }

                        break;
                    }
                    case 3: {
                        arrayOfMatrices[countMatrices - 1] = createMatrix(size, sizeof(double));
                        printf("Enter matrix elements:\n");
                        double ***elements = malloc(2 * sizeof(double **));
                        for (int i = 0; i < size; ++i) {
                            elements[i] = malloc(size * sizeof(double *));
                            for (int j = 0; j < size; ++j) {
                                double *e = malloc(sizeof(double));
                                scanf("%lf", e);
                                elements[i][j] = e;
                            }
                        }

                        int res = addElements(arrayOfMatrices[countMatrices - 1],
                                              (void ***) elements,
                                              sizeof(double),
                                              &DoubleField);
                        if (res != 0) {
                            if (res == MATRIX_NULL) {
                                printf("Matrix %d is null\n", countMatrices - 1);
                            } else if (res == FAILED_MALLOC) {
                                printf("Failed to allocate memory\n");
                            } else if (res == ELEMENTS_ARRAY_NULL) {
                                printf("Elements array is null\n");
                            }
                        } else {
                            printf("Matrix %d created successfully\n", countMatrices);
                        }

                        break;
                    }
                    case 4: {
                        arrayOfMatrices[countMatrices - 1] = createMatrix(size, sizeof(long));
                        printf("Enter matrix elements:\n");
                        long ***elements = malloc(2 * sizeof(long **));
                        for (int i = 0; i < size; ++i) {
                            elements[i] = malloc(size * sizeof(long *));
                            for (int j = 0; j < size; ++j) {
                                long *e = malloc(sizeof(long));
                                scanf("%ld", e);
                                elements[i][j] = e;
                            }
                        }

                        int res = addElements(arrayOfMatrices[countMatrices - 1],
                                              (void ***) elements,
                                              sizeof(long),
                                              &LongField);
                        if (res != 0) {
                            if (res == MATRIX_NULL) {
                                printf("Matrix %d is null\n", countMatrices - 1);
                            } else if (res == FAILED_MALLOC) {
                                printf("Failed to allocate memory\n");
                            } else if (res == ELEMENTS_ARRAY_NULL) {
                                printf("Elements array is null\n");
                            }
                        } else {
                            printf("Matrix %d created successfully\n", countMatrices);
                        }
                        break;
                    }
                    default: {
                        break;
                    }
                }

                break;
            }
            case 2: {
                if (countMatrices == 0) {
                    printf("There are no matrices\n");
                    break;
                }
                int index;
                printf("Enter index of matrix: ");
                scanf("%d", &index);
                while (index < 1 || index > countMatrices) {
                    printf("Enter an index between 1 and %d: ", countMatrices);
                    scanf("%d", &index);
                }
                int log = destroyMatrix(arrayOfMatrices[index - 1]);
                if (log != OK){
                    printf("error: %d\n", log);
                } else {
                    countMatrices--;
                    arrayOfMatrices = (Matrix**)realloc(arrayOfMatrices, countMatrices * sizeof(Matrix*));
                }
                printf("Matrix %d deleted successfully\n", index);
                break;
            }
            case 3: {
                if (countMatrices == 0) {
                    printf("There are no matrices\n");
                    break;
                }

                int index1;
                printf("Enter index of first matrix: ");
                scanf("%d", &index1);
                while (index1 < 1 || index1 > countMatrices) {
                    printf("Enter an index between 1 and %d: ", countMatrices);
                    scanf("%d", &index1);
                }

                int index2;
                printf("Enter index of second matrix: ");
                scanf("%d", &index2);
                while (index2 < 1 || index2 > countMatrices) {
                    printf("Enter an index between 1 and %d: ", countMatrices);
                    scanf("%d", &index2);
                }
                pMatrix result = createMatrix(arrayOfMatrices[index1 - 1]->size,
                                              arrayOfMatrices[index1 - 1]->elementSize);
                int log = matrixSum(arrayOfMatrices[index1 - 1], arrayOfMatrices[index2 - 1], result);
                if (log != OK) {
                    printf("error: %d\n", log);
                    break;
                }
                printf("Sum of matrices %d and %d:\n", index1, index2);
                log = printMatrix(result);
                if (log != OK){
                    printf("error: %d\n", log);
                }
                break;
            }
            case 4: {
                if (countMatrices == 0) {
                    printf("There are no matrices\n");
                    break;
                }

                int index1;
                printf("Enter index of first matrix: ");
                scanf("%d", &index1);
                while (index1 < 1 || index1 > countMatrices) {
                    printf("Enter an index between 1 and %d: ", countMatrices);
                    scanf("%d", &index1);
                }

                int index2;
                printf("Enter index of second matrix: ");
                scanf("%d", &index2);
                while (index2 < 1 || index2 > countMatrices) {
                    printf("Enter an index between 1 and %d: ", countMatrices);
                    scanf("%d", &index2);
                }

                pMatrix result = createMatrix(arrayOfMatrices[index1 - 1]->size,
                                              arrayOfMatrices[index1 - 1]->elementSize);

                int log = matrixMultiply(arrayOfMatrices[index1 - 1], arrayOfMatrices[index2 - 1], result);
                if (log != OK) {
                    printf("error: %d\n", log);
                }
                printf("Product of matrices %d and %d:\n", index1, index2);
                log = printMatrix(result);
                if (log != OK){
                    printf("error: %d\n", log);
                }

                log = destroyMatrix(result);
                if (log != OK){
                    printf("error: %d\n", log);
                }
                break;
            }
            case 5: {
                if (countMatrices == 0) {
                    printf("There are no matrices\n");
                    break;
                }
                int matrixIndex;
                printf("Enter index of matrix: ");
                scanf("%d", &matrixIndex);
                while (matrixIndex < 1 || matrixIndex > countMatrices) {
                    printf("Enter an index between 1 and %d: ", countMatrices);
                    scanf("%d", &matrixIndex);
                }

                int vectorIndex;
                printf("Enter index of the vector: ");
                scanf("%d", &vectorIndex);
                while (vectorIndex < 0 || vectorIndex >= arrayOfMatrices[matrixIndex - 1]->size) {
                    printf("Enter an index between 0 and %zu: ", arrayOfMatrices[matrixIndex - 1]->size);
                    scanf("%d", &vectorIndex);
                }

                int useStringOrColumn;
                printf("Choose operation (0 - Add to column, 1 - Add to string): ");
                scanf("%d", &useStringOrColumn);
                while (useStringOrColumn != 1 && useStringOrColumn != 2) {
                    printf("Invalid operation. Choose 0 or 1: ");
                    scanf("%d", &useStringOrColumn);
                }

                int resultVectorIndex;
                printf("Enter index of the result vector: ");
                scanf("%d", &resultVectorIndex);
                while (resultVectorIndex < 0 || resultVectorIndex > arrayOfMatrices[matrixIndex - 1]->size) {
                    printf("Enter an index between 1 and %zu: ", arrayOfMatrices[matrixIndex - 1]->size);
                    scanf("%d", &resultVectorIndex);
                }

                int number_type;
                printf("Enter number type:\n 1 - int,\n 2 - float,\n 3 - double,\n 4 - long\n");
                scanf("%d", &number_type);
                while (number_type < 1 || number_type > 4) {
                    printf("Enter valid number type:\n 1 - int\n, 2 - float\n, 3 - double\n, 4 - long\n");
                    scanf("%d", &number_type);
                }
                void* number = NULL;
                switch (number_type) {
                    case 1:
                        number = malloc(sizeof(int));
                        printf("Enter number: ");
                        scanf("%d", (int*)number);
                        break;
                    case 2:
                        number = malloc(sizeof(float));
                        printf("Enter number: ");
                        scanf("%f", (float*)number);
                        break;
                    case 3:
                        number = malloc(sizeof(double));
                        printf("Enter number: ");
                        scanf("%lf", (double*)number);
                        break;
                    case 4:
                        number = malloc(sizeof(long));
                        printf("Enter number: ");
                        scanf("%ld", (long*)number);
                        break;
                    default: {
                        break;
                    }
                }
                int log = addLinearCombination(arrayOfMatrices[matrixIndex - 1], vectorIndex,
                                               useStringOrColumn, number, resultVectorIndex);
                if (log != OK) {
                    printf("error: %d\n", log);
                } else {
                    printf("Linear combination added successfully.\n");
                }
                free(number);
                break;
            }
            case 6: {
                if (countMatrices == 0) {
                    printf("There are no matrices\n");
                    break;
                }

                int index;
                printf("Enter index of matrix: ");
                scanf("%d", &index);
                while (index < 1 || index > countMatrices) {
                    printf("Enter an index between 1 and %d: ", countMatrices);
                    scanf("%d", &index);
                }

                int number_type;
                printf("Enter number type:\n 1 - int,\n 2 - float,\n 3 - double,\n 4 - long\n");
                scanf("%d", &number_type);
                while (number_type < 1 || number_type > 4) {
                    printf("Enter valid number type:\n 1 - int\n, 2 - float\n, 3 - double\n, 4 - long\n");
                    scanf("%d", &number_type);
                }
                void* number = NULL;
                switch (number_type) {
                    case 1:
                        number = malloc(sizeof(int));
                        printf("Enter number: ");
                        scanf("%d", (int*)number);
                        break;
                    case 2:
                        number = malloc(sizeof(float));
                        printf("Enter number: ");
                        scanf("%f", (float*)number);
                        break;
                    case 3:
                        number = malloc(sizeof(double));
                        printf("Enter number: ");
                        scanf("%lf", (double*)number);
                        break;
                    case 4:
                        number = malloc(sizeof(long));
                        printf("Enter number: ");
                        scanf("%ld", (long*)number);
                        break;
                    default: {
                        break;
                    }
                }

                int log = multiplyByNumber(arrayOfMatrices[index - 1], number);
                if (log != OK) {
                    printf("error: %d\n", log);
                } else {
                    printf("Matrix multiplied successfully.\n");
                }
                break;
            }
            case 7: {
                if (countMatrices == 0) {
                    printf("\nThere are no matrices\n");
                    break;
                }
                for (int i = 0; i < countMatrices; ++i) {
                    printf("\nMatrix %d:\n", i+1);
                    int log = printMatrix(arrayOfMatrices[i]);
                    if (log != OK){
                        printf("error: %d\n", log);
                    }
                }
            }
            default:{
                printf("Invalid choice\n");
            }
        }
    }
    int log;
    for (int i = 0; i < countMatrices; ++i) {
        log = destroyMatrix(arrayOfMatrices[i]);
        if (log != OK){
            printf("error: %d\n", log);
            break;
        }
    }
    if (countMatrices != 0){
        free(arrayOfMatrices);
    }
}
