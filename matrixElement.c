#include <stdio.h>
#include <stdlib.h>
#include "MatrixElement.h"

// Create a matrix element
MatrixElement* createMatrixElement(void* value, size_t size, Field* field) {
    if (value == NULL) return NULL;

    MatrixElement* element = (MatrixElement*)malloc(sizeof(MatrixElement));
    if (element == NULL) return NULL;

    element->value = value;
    element->size = size;
    element->field = field;
    return element;
}