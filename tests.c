#include <stdlib.h>
#include <assert.h>
#include <limits.h>
#include "Matrix.h"
#include "ComplexNumber.h"
#include "LoggingFlags.h"

void testCreateMatrix() {
    // Test Cases for createMatrix:

    // Test creating a matrix with a valid size and element size
    pMatrix matrix1 = createMatrix(3, sizeof(int));
    assert(matrix1 != NULL);
    printf("Test 1: Matrix with valid size and element size created successfully.\n");

    // Test creating a matrix with a negative size
    pMatrix matrix2 = createMatrix(-5, sizeof(int));
    assert(matrix2 == NULL);
    printf("Test 2: Matrix creation failed for negative size.\n");

    // Test creating a matrix with a zero size
    pMatrix matrix3 = createMatrix(0, sizeof(int));
    assert(matrix3 == NULL);
    printf("Test 3: Matrix creation failed for zero size.\n");

    // Free allocated memory
    int d = destroyMatrix(matrix1);
    assert(d == OK);
}

void testDestroyMatrix() {
    // Test destroying a valid matrix

    pMatrix matrix = createMatrix(3, sizeof(int));
    int result1 = destroyMatrix(matrix);
    assert(result1 == OK);
    printf("Test 4: Valid matrix destroyed successfully.\n");

    // Test destroying a NULL matrix pointer
    pMatrix nullMatrix = NULL;
    int result2 = destroyMatrix(nullMatrix);
    assert(result2 == MATRIX_NULL);
    printf("Test 5: NULL matrix pointer handled correctly.\n");
}

void testPrintMatrix() {
    // Test Cases for printMatrix:

    // Create a sample matrix for testing
    pMatrix matrix1 = createMatrix(2, sizeof(int));
    int value = 1;
    for (int i = 0; i < 2; ++i) {
        for (int j = 0; j < 2; ++j) {
            int* element = malloc(sizeof(int));
            *element = value;
            MatrixElement matrixElement = {element, sizeof(int), &IntField};
            matrix1->elements[i][j] = matrixElement;
            value+=1;
        }
    }

    // Test printing a valid matrix
    printf("\n");
    int result6 = printMatrix(matrix1);
    assert(result6 == OK);
    printf("Test 6: Printing a valid matrix completed successfully.\n\n");

    // Test printing a NULL matrix pointer
    pMatrix nullMatrix = NULL;
    int result7 = printMatrix(nullMatrix);
    assert(result7 == MATRIX_NULL);
    printf("Test 7: Printing a NULL matrix pointer handled correctly.\n");

    // Test printing a matrix with NULL elements array
    free(matrix1->elements); // Simulating NULL elements array
    matrix1->elements = NULL;
    int result8 = printMatrix(matrix1);
    assert(result8 == ELEMENTS_ARRAY_NULL);
    printf("Test 8: Printing a matrix with NULL elements array handled correctly.\n");

    // Test printing a matrix with a NULL element
    pMatrix matrix2 = createMatrix(2, sizeof(int));
    for (int i = 1; i < 3; ++i) {
        for (int j = 1; j < 3; ++j) {
            int* element = malloc(sizeof(int));
            *element = i+j;
            MatrixElement matrixElement = {element, sizeof(int), &IntField};
            matrix2->elements[i - 1][j - 1] = matrixElement;
        }
    }

    // Simulating an element with NULL value
    matrix2->elements[1][1].value = NULL;
    int result4 = printMatrix(matrix2);
    assert(result4 == VALUE_NULL);
    printf("Test 9: Printing a matrix with a NULL element handled correctly.\n");

    // Free allocated memory
    int d1 = destroyMatrix(matrix1);
    assert(d1 == OK);
    int d2 = destroyMatrix(matrix2);
    assert(d2 == OK);
}

// Test adding elements to a matrix of each supported type
void testAddElements_OK() {
    // Create a sample matrix for testing
    pMatrix intMatrix = createMatrix(2, sizeof(int));
    assert(intMatrix != NULL);

    int*** elements = malloc(2 * sizeof(int**));
    elements[0] = malloc(2 * sizeof(int*));
    elements[1] = malloc(2 * sizeof(int*));
    int* a = malloc(sizeof(int));
    *a = 1;
    int* b = malloc(sizeof(int));
    *b = 2;
    int* c = malloc(sizeof(int));
    *c = 3;
    int* d = malloc(sizeof(int));
    *d = 4;
    elements[0][0] = a;
    elements[0][1] = b;
    elements[1][0] = c;
    elements[1][1] = d;

    // Test adding int elements
    int intResult = addElements(intMatrix, (void ***)elements, sizeof(int), &IntField);
    assert(intResult == OK);

    printf("Test 10: Adding elements to a matrix of each supported type completed successfully.\n");

    // Free allocated memory
    int d1 = destroyMatrix(intMatrix);
    assert(d1 == OK);
}

// Test adding elements to a NULL matrix pointer
void testAddElements_NullMatrixPointer() {
    // Test adding elements to a NULL matrix pointer
    pMatrix nullMatrix = NULL;
    int intResult = addElements(nullMatrix, NULL, 1, &IntField);
    assert(intResult == MATRIX_NULL);

    printf("Test 11: Adding elements to a NULL matrix pointer handled correctly.\n");
}

// Test setting an element at a valid index with a valid element
void testSetElement_OK() {
    // Create a sample matrix for testing
    pMatrix matrix = createMatrix(2, sizeof(int));
    assert(matrix != NULL);

    // Create a valid element
    int *value = malloc(sizeof(*value));
    assert(value != NULL);
    *value = 42;
    MatrixElement *element = createMatrixElement(value, sizeof(int), &IntField);
    assert(element != NULL);

    // Test setting the element
    int result = setElement(matrix, 0, 0, *element);
    assert(result == OK);
    printf("Test 12: Setting an element at a valid index with a valid element completed successfully.\n");

    // Free allocated memory
    int d = destroyMatrix(matrix);
    assert(d == OK);
}

// Test setting an element at an invalid index
void testSetElement_InvalidIndex() {
    // Create a sample matrix for testing
    pMatrix matrix = createMatrix(2, sizeof(int));
    assert(matrix != NULL);

    // Create a valid element
    int *value = malloc(sizeof(*value));
    assert(value != NULL);
    *value = 42;
    MatrixElement *element = createMatrixElement(value, sizeof(int), &IntField);
    assert(element != NULL);

    // Test setting the element at an invalid index
    int result = setElement(matrix, 2, 2, *element); // Index out of bounds
    assert(result == INVALID_INDEX);
    printf("Test 13: Setting an element at an invalid index handled correctly.\n");

    // Free allocated memory
    int d1 = destroyMatrix(matrix);
    assert(d1 == OK);
}

// Test setting an element with a NULL matrix pointer
void testSetElement_NullMatrixPointer() {
    // Test setting an element with a NULL matrix pointer
    pMatrix nullMatrix = NULL;
    int* value = malloc(sizeof(*value));
    assert(value != NULL);
    *value = 42;
    MatrixElement element = { .value = value, .size = sizeof(int), .field = &IntField }; // Sample element
    int result = setElement(nullMatrix, 0, 0, element);
    assert(result == MATRIX_NULL);
    printf("Test 14: Setting an element with a NULL matrix pointer handled correctly.\n");

    // Free allocated memory
    free(element.value);
}

// Test setting an element with a NULL element
void testSetElement_NullElement() {
    // Create a sample matrix for testing
    pMatrix matrix = createMatrix(2, sizeof(int));
    assert(matrix->elements != NULL);

    // Test setting an element with a NULL element
    MatrixElement nullElement = { .value = NULL }; // A NULL element
    int result = setElement(matrix, 0, 0, nullElement);
    assert(result == ELEMENT_NULL);

    // Free allocated memory
    int d = destroyMatrix(matrix);
    assert(d == OK);
}

// Test getting an element at a valid index
void testGetElement_OK() {
    // Create a sample matrix for testing
    pMatrix matrix = createMatrix(2, sizeof(int));
    assert(matrix != NULL);

    // Create a sample element
    int *value = malloc(sizeof(*value));
    assert(value != NULL);
    *value = 42;
    matrix->elements[0][0] = *createMatrixElement(value, sizeof(int), &IntField);
    assert(matrix->elements[0][0].value != NULL);

    // Get the element
    MatrixElement *pElement = NULL;
    int result = getElement(matrix, 0, 0, &pElement);
    assert(result == OK);
    assert(pElement != NULL);
    assert(*(int *)pElement->value == *value);
    printf("Test 15: Getting an element at a valid index completed successfully.\n");

    // Free allocated memory
    int d = destroyMatrix(matrix);
    assert(d == OK);
}

// Test getting an element at an invalid index
void testGetElement_InvalidIndex() {
    // Create a sample matrix for testing
    pMatrix matrix = createMatrix(2, sizeof(int));
    assert(matrix != NULL);

    // Get the element at an invalid index
    MatrixElement *pElement = NULL;
    int result = getElement(matrix, 2, 2, &pElement); // Index out of bounds
    assert(result == INVALID_INDEX);
    assert(pElement == NULL);
    printf("Test 16: Getting an element at an invalid index handled correctly.\n");

    // Free allocated memory
    int d = destroyMatrix(matrix);
    assert(d == OK);
}

// Test getting an element from a NULL matrix pointer
void testGetElement_NullMatrixPointer() {
    // Test getting an element from a NULL matrix pointer
    pMatrix nullMatrix = NULL;
    MatrixElement *pElement = NULL;
    int result = getElement(nullMatrix, 0, 0, &pElement);
    assert(result == MATRIX_NULL);
    assert(pElement == NULL);
    printf("Test 17: Getting an element from a NULL matrix pointer handled correctly.\n");
}

// Test cases for matrix sum
void testMatrixSum_OK() {
    pMatrix matrix1 = createMatrix(2, sizeof(int));
    pMatrix matrix2 = createMatrix(2, sizeof(int));
    for (int i = 0; i < 2; ++i) {
        for (int j = 0; j < 2; ++j) {
            int* element = malloc(sizeof(int));
            *element = 1;
            MatrixElement matrixElement = *createMatrixElement(element, sizeof(int), &IntField);
            matrix1->elements[i][j] = matrixElement;
        }
    }
    for (int i = 0; i < 2; ++i) {
        for (int j = 0; j < 2; ++j) {
            int* element = malloc(sizeof(int));
            *element = 2;
            MatrixElement matrixElement = *createMatrixElement(element, sizeof(int), &IntField);
            matrix2->elements[i][j] = matrixElement;
        }
    }

    pMatrix resultMatrix = createMatrix(2, sizeof(int));
    for (int i = 0; i < 2; ++i) {
        for (int j = 0; j < 2; ++j) {
            int* element = malloc(sizeof(int));
            *element = 0;
            MatrixElement matrixElement = *createMatrixElement(element, sizeof(int), &IntField);
            resultMatrix->elements[i][j] = matrixElement;
        }
    }

    int result = matrixSum(matrix1, matrix2, resultMatrix);
    assert(result == OK);
    for (int i = 0; i < 2; ++i) {
        for (int j = 0; j < 2; ++j) {
            assert(*(int*)(resultMatrix->elements[i][j].value) == 3);
        }
    }
    printf("Test 18: Matrix sum completed successfully.\n");

    // Free allocated memory
    int d1 = destroyMatrix(matrix1);
    assert(d1 == OK);
    int d2 = destroyMatrix(matrix2);
    assert(d2 == OK);
    int d3 = destroyMatrix(resultMatrix);
    assert(d3 == OK);
}

// Test summing matrices with incompatible sizes
void testMatrixSum_IncompatibleSizes() {
    pMatrix matrix1 = createMatrix(2, sizeof(int));
    pMatrix matrix2 = createMatrix(3, sizeof(int));
    pMatrix matrixResult = createMatrix(3, sizeof(int));

    int result = matrixSum(matrix1, matrix2, matrixResult);
    assert(result == INCOMPATIBLE_SIZE);
    printf("Test 19: Matrix sum with incompatible sizes handled correctly.\n");

    // Free allocated memory
    int d1 = destroyMatrix(matrix1);
    assert(d1 == OK);
    int d2 = destroyMatrix(matrix2);
    assert(d2 == OK);
    int d3 = destroyMatrix(matrixResult);
    assert(d3 == OK);
}

// Test summing matrices with NULL pointers
void testMatrixSum_NullPointers() {
    int result = matrixSum(NULL, NULL, NULL);
    assert(result == MATRIX_NULL);
    printf("Test 20: Matrix sum with NULL matrices handled correctly.\n");

    pMatrix matrix1 = createMatrix(2, sizeof(int));
    matrix1->elements = NULL;

    pMatrix matrix2 = createMatrix(2, sizeof(int));
    pMatrix matrixResult = createMatrix(2, sizeof(int));

    result = matrixSum(matrix1, matrix2, matrixResult);
    assert(result == ELEMENTS_ARRAY_NULL);
    printf("Test 21: Matrix sum with NULL elements array handled correctly.\n");

    pMatrix matrix3 = createMatrix(2, sizeof(int));
    pMatrix matrix4 = createMatrix(2, sizeof(int));
    for (int i = 0; i < 2; ++i) {
        for (int j = 0; j < 2; ++j) {
            int* element = malloc(sizeof(int));
            *element = 1;
            MatrixElement matrixElement = *createMatrixElement(element, sizeof(int), &IntField);
            matrix3->elements[i][j] = matrixElement;
        }
    }
    for (int i = 0; i < 2; ++i) {
        for (int j = 0; j < 2; ++j) {
            int* element = malloc(sizeof(int));
            *element = 2;
            MatrixElement matrixElement = *createMatrixElement(element, sizeof(int), &IntField);
            matrix4->elements[i][j] = matrixElement;
        }
    }

    pMatrix resultMatrix = createMatrix(2, sizeof(int));
    for (int i = 0; i < 2; ++i) {
        for (int j = 0; j < 2; ++j) {
            int* element = malloc(sizeof(int));
            *element = 0;
            MatrixElement matrixElement = *createMatrixElement(element, sizeof(int), &IntField);
            resultMatrix->elements[i][j] = matrixElement;
        }
    }

    matrix3->elements[0][0].value = NULL;
    result = matrixSum(matrix3, matrix4, resultMatrix);
    assert(result == VALUE_NULL);
    printf("Test 22: Matrix sum with NULL value handled correctly.\n");

    // Free allocated memory
    int d1 = destroyMatrix(matrix1);
    assert(d1 == OK);
    int d2 = destroyMatrix(matrix2);
    assert(d2 == OK);
    int d3 = destroyMatrix(matrixResult);
    assert(d3 == OK);
    int d4 = destroyMatrix(matrix3);
    assert(d4 == OK);
    int d5 = destroyMatrix(matrix4);
    assert(d5 == OK);
    int d6 = destroyMatrix(resultMatrix);
    assert(d6 == OK);
}

// Test summing matrices resulting in overflow
void testMatrixSum_Overflow() {
    pMatrix matrix1 = createMatrix(2, sizeof(int));
    pMatrix matrix2 = createMatrix(2, sizeof(int));
    for (int i = 0; i < 2; ++i) {
        for (int j = 0; j < 2; ++j) {
            int* element = malloc(sizeof(int));
            *element = INT_MAX/2;
            MatrixElement matrixElement = *createMatrixElement(element, sizeof(int), &IntField);
            matrix1->elements[i][j] = matrixElement;
        }
    }
    for (int i = 0; i < 2; ++i) {
        for (int j = 0; j < 2; ++j) {
            int* element = malloc(sizeof(int));
            *element = INT_MAX/2+1234;
            MatrixElement matrixElement = *createMatrixElement(element, sizeof(int), &IntField);
            matrix2->elements[i][j] = matrixElement;
        }
    }

    pMatrix resultMatrix = createMatrix(2, sizeof(int));
    for (int i = 0; i < 2; ++i) {
        for (int j = 0; j < 2; ++j) {
            int* element = malloc(sizeof(int));
            *element = 0;
            MatrixElement matrixElement = *createMatrixElement(element, sizeof(int), &IntField);
            resultMatrix->elements[i][j] = matrixElement;
        }
    }

    int result = matrixSum(matrix1, matrix2, resultMatrix);
    assert(result == SUM_OVERFLOW);
    printf("Test 23: Matrix sum with overflow handled correctly.\n");

    // Free allocated memory
    int d1 = destroyMatrix(matrix1);
    assert(d1 == OK);
    int d2 = destroyMatrix(matrix2);
    assert(d2 == OK);
    int d3 = destroyMatrix(resultMatrix);
    assert(d3 == OK);
}

// Test multiplying a matrix by a valid number
void testMultiplyByNumber_ValidNumber() {
    // Initialize a matrix
    pMatrix matrix = createMatrix(2, sizeof(int));
    assert(matrix != NULL);
    for (int i = 0; i < 2; ++i) {
        for (int j = 0; j < 2; ++j) {
            int* element = malloc(sizeof(int));
            *element = 1;
            MatrixElement matrixElement = *createMatrixElement(element, sizeof(int), &IntField);
            matrix->elements[i][j] = matrixElement;
        }
    }

    // Perform multiplication by a valid number
    int number = 2;
    int result = multiplyByNumber(matrix, &number);
    assert(result == OK);
    printf("Test 24: Matrix multiplication by a valid number handled correctly.\n");

    // Free allocated memory
    int d1 = destroyMatrix(matrix);
    assert(d1 == OK);
}

// Test multiplying a matrix by a NULL number
void testMultiplyByNumber_NullNumber() {
    // Initialize a matrix
    pMatrix matrix = createMatrix(2, sizeof(int));
    assert(matrix != NULL);

    // Perform multiplication by a NULL number
    int result = multiplyByNumber(matrix, NULL);
    assert(result == ELEMENT_NULL);
    printf("Test 25: Matrix multiplication by a NULL number handled correctly.\n");

    // Free allocated memory
    destroyMatrix(matrix);
}

// Test multiplying a matrix with a NULL pointer
void testMultiplyByNumber_NullPointer() {
    // Perform multiplication with a NULL matrix pointer
    int number = 2;
    int result = multiplyByNumber(NULL, &number);
    assert(result == MATRIX_NULL);
    printf("Test 26: Matrix multiplication with a NULL matrix pointer handled correctly.\n");
}

// Test multiplication resulting in overflow
void testMultiplyByNumber_Overflow() {
    // Initialize a matrix
    pMatrix matrix = createMatrix(2, sizeof(int));
    assert(matrix != NULL);
    for (int i = 0; i < 2; ++i) {
        for (int j = 0; j < 2; ++j) {
            int* element = malloc(sizeof(int));
            *element = INT_MAX/2;
            MatrixElement matrixElement = *createMatrixElement(element, sizeof(int), &IntField);
            matrix->elements[i][j] = matrixElement;
        }
    }

    // Perform multiplication
    int number = INT_MAX;
    int result = multiplyByNumber(matrix, &number);
    assert(result == MUL_OVERFLOW); // Expecting overflow
    printf("Test 27: Matrix multiplication resulting in overflow handled correctly.\n");

    // Free allocated memory
    destroyMatrix(matrix);
}

// Test multiplying two matrices with compatible sizes
void testMatrixMultiply_OK() {
    // Initialize matrices with compatible sizes
    pMatrix matrix1 = createMatrix(2, sizeof(int));
    assert(matrix1 != NULL);

    pMatrix matrix2 = createMatrix(2, sizeof(int));
    assert(matrix2 != NULL);

    pMatrix result = createMatrix(2, sizeof(int));
    assert(result != NULL);

    // Set the elements of the matrices
    for (int i = 0; i < 2; ++i) {
        for (int j = 0; j < 2; ++j) {
            int* element1 = malloc(sizeof(int));
            int* element2 = malloc(sizeof(int));
            *element1 = 1;
            *element2 = 2;
            MatrixElement matrixElement1 = *createMatrixElement(element1, sizeof(int), &IntField);
            MatrixElement matrixElement2 = *createMatrixElement(element2, sizeof(int), &IntField);
            matrix1->elements[i][j] = matrixElement1;
            matrix2->elements[i][j] = matrixElement2;
        }
    }

    // Set the elements of the result matrix
    for (int i = 0; i < 2; ++i) {
        for (int j = 0; j < 2; ++j) {
            int* element = malloc(sizeof(int));
            *element = 0;
            MatrixElement matrixElement = *createMatrixElement(element, sizeof(int), &IntField);
            result->elements[i][j] = matrixElement;
        }
    }

    // Perform matrix multiplication
    int multiplyResult = matrixMultiply(matrix1, matrix2, result);
    assert(multiplyResult == OK);
    printf("Test 28: Matrix multiplication with compatible sizes handled correctly.\n");

    // Free allocated memory
    int d1 = destroyMatrix(matrix1);
    assert(d1 == OK);
    int d2 = destroyMatrix(matrix2);
    assert(d2 == OK);
    int d3 = destroyMatrix(result);
    assert(d3 == OK);
}

// Test multiplying matrices with incompatible sizes
void testMatrixMultiply_IncompatibleSizes() {
    // Initialize matrices with incompatible sizes
    pMatrix matrix1 = createMatrix(2, sizeof(int));
    assert(matrix1 != NULL);

    pMatrix matrix2 = createMatrix(3, sizeof(int));
    assert(matrix2 != NULL);

    pMatrix result = createMatrix(2, sizeof(int));
    assert(result != NULL);

    // Perform matrix multiplication
    int multiplyResult = matrixMultiply(matrix1, matrix2, result);
    assert(multiplyResult == INCOMPATIBLE_SIZE);
    printf("Test 29: Matrix multiplication with incompatible sizes handled correctly.\n");
    // Free allocated memory
    int d1 = destroyMatrix(matrix1);
    assert(d1 == OK);
    int d2 = destroyMatrix(matrix2);
    assert(d2 == OK);
    int d3 = destroyMatrix(result);
    assert(d3 == OK);
}

// Test multiplying matrices with NULL pointers
void testMatrixMultiply_NullPointers() {
    // Perform matrix multiplication with NULL pointers
    pMatrix matrix1 = NULL;
    pMatrix matrix2 = NULL;
    pMatrix result = NULL;

    int multiplyResult = matrixMultiply(matrix1, matrix2, result);
    assert(multiplyResult == MATRIX_NULL);
    printf("Test 30: Matrix multiplication with NULL pointers handled correctly.\n");
}

// Test multiplication resulting in overflow
void testMatrixMultiply_Overflow() {
    // Initialize matrices with compatible sizes
    pMatrix matrix1 = createMatrix(2, sizeof(int));
    assert(matrix1 != NULL);

    pMatrix matrix2 = createMatrix(2, sizeof(int));
    assert(matrix2 != NULL);

    pMatrix result = createMatrix(2, sizeof(int));
    assert(result != NULL);

    for (int i = 0; i < 2; ++i) {
        for (int j = 0; j < 2; ++j) {
            int* element1 = malloc(sizeof(int));
            int* element2 = malloc(sizeof(int));
            *element1 = INT_MAX/2;
            *element2 = INT_MAX/2;
            MatrixElement matrixElement1 = *createMatrixElement(element1, sizeof(int), &IntField);
            MatrixElement matrixElement2 = *createMatrixElement(element2, sizeof(int), &IntField);
            matrix1->elements[i][j] = matrixElement1;
            matrix2->elements[i][j] = matrixElement2;
        }
    }

    // Set the elements of the result matrix
    for (int i = 0; i < 2; ++i) {
        for (int j = 0; j < 2; ++j) {
            int* element = malloc(sizeof(int));
            *element = 0;
            MatrixElement matrixElement = *createMatrixElement(element, sizeof(int), &IntField);
            result->elements[i][j] = matrixElement;
        }
    }

    // Perform matrix multiplication
    int multiplyResult = matrixMultiply(matrix1, matrix2, result);
    assert(multiplyResult == MUL_OVERFLOW || SUM_OVERFLOW);
    printf("Test 31: Matrix multiplication resulting in overflow handled correctly.\n");

    // Free allocated memory
    int d1 = destroyMatrix(matrix1);
    assert(d1 == OK);
    int d2 = destroyMatrix(matrix2);
    assert(d2 == OK);
    int d3 = destroyMatrix(result);
    assert(d3 == OK);
}

// Test adding a linear combination to a matrix string with a valid number and index
void testAddLinearCombination_String_Valid() {
    pMatrix matrix = createMatrix(2, sizeof(int));
    assert(matrix != NULL);

    // Set the elements of the matrix
    for (int i = 0; i < 2; ++i) {
        for (int j = 0; j < 2; ++j) {
            int* element = malloc(sizeof(int));
            *element = 1;
            MatrixElement matrixElement = *createMatrixElement(element, sizeof(int), &IntField);
            matrix->elements[i][j] = matrixElement;
        }
    }

    // Perform adding a linear combination to a string
    int number = 2;
    int result = addLinearCombination(matrix, 0, 1, &number, 1);
    assert(result == OK);

    printf("Test 32: Adding a linear combination to a matrix string handled correctly.\n");

    // Free allocated memory
    int d1 = destroyMatrix(matrix);
    assert(d1 == OK);
}

// Test adding a linear combination to a matrix column with a valid number and index
void testAddLinearCombination_Column_Valid() {
    pMatrix matrix = createMatrix(2, sizeof(int));
    assert(matrix != NULL);

    // Set the elements of the matrix
    for (int i = 0; i < 2; ++i) {
        for (int j = 0; j < 2; ++j) {
            int* element = malloc(sizeof(int));
            *element = 1;
            MatrixElement matrixElement = *createMatrixElement(element, sizeof(int), &IntField);
            matrix->elements[i][j] = matrixElement;
        }
    }

    int number = 2;
    int result = addLinearCombination(matrix, 0, 0, &number, 1);
    assert(result == OK);

    printf("Test 33: Adding a linear combination to a matrix column handled correctly.\n");

    // Free allocated memory
    int d = destroyMatrix(matrix);
    assert(d == OK);
}

// Test adding a linear combination with a NULL matrix pointer
void testAddLinearCombination_NullMatrixPointer() {
    int number = 2;
    int result = addLinearCombination(NULL, 0, 1, &number, 1);
    assert(result == MATRIX_NULL);
    printf("Test 34: Adding a linear combination with a NULL matrix pointer handled correctly.\n");
}

// Test adding a linear combination with a NULL number
void testAddLinearCombination_NullNumber() {
    pMatrix matrix = createMatrix(2, sizeof(int));
    assert(matrix != NULL);

    for (int i = 0; i < 2; ++i) {
        for (int j = 0; j < 2; ++j) {
            int* element = malloc(sizeof(int));
            *element = 1;
            MatrixElement matrixElement = *createMatrixElement(element, sizeof(int), &IntField);
            matrix->elements[i][j] = matrixElement;
        }
    }

    int result = addLinearCombination(matrix, 0, 1, NULL, 1);
    assert(result == ELEMENT_NULL);

    printf("Test 35: Adding a linear combination with a NULL number handled correctly.\n");

    // Free allocated memory
    int d = destroyMatrix(matrix);
    assert(d == OK);
}

// Test adding a linear combination with an invalid index
void testAddLinearCombination_InvalidIndex() {
    pMatrix matrix = createMatrix(2, sizeof(int));
    assert(matrix != NULL);

    for (int i = 0; i < 2; ++i) {
        for (int j = 0; j < 2; ++j) {
            int* element = malloc(sizeof(int));
            *element = 1;
            MatrixElement matrixElement = *createMatrixElement(element, sizeof(int), &IntField);
            matrix->elements[i][j] = matrixElement;
        }
    }

    int number = 2;
    int result = addLinearCombination(matrix, -1, 1, &number, 1);
    assert(result == INVALID_INDEX);

    printf("Test 36: Adding a linear combination with an invalid index handled correctly.\n");

    // Free allocated memory
    int d = destroyMatrix(matrix);
    assert(d == OK);
}


int test() {
    // Test create matrix
    testCreateMatrix();

    // Test destroy matrix
    testDestroyMatrix();

    // Test print matrix
    testPrintMatrix();

    // Tests add elements
    testAddElements_OK();
    testAddElements_NullMatrixPointer();

    // Tests set element
    testSetElement_OK();
    testSetElement_InvalidIndex();
    testSetElement_NullMatrixPointer();
    testSetElement_NullElement();

    // Tests get element
    testGetElement_OK();
    testGetElement_InvalidIndex();
    testGetElement_NullMatrixPointer();

    // Tests matrix sum
    testMatrixSum_OK();
    testMatrixSum_IncompatibleSizes();
    testMatrixSum_NullPointers();
    testMatrixSum_Overflow();

    // Tests multiply by number
    testMultiplyByNumber_ValidNumber();
    testMultiplyByNumber_NullNumber();
    testMultiplyByNumber_NullPointer();
    testMultiplyByNumber_Overflow();

    // Tests matrix multiplication
    testMatrixMultiply_OK();
    testMatrixMultiply_IncompatibleSizes();
    testMatrixMultiply_NullPointers();
    testMatrixMultiply_Overflow();

    // Tests add linear combination
    testAddLinearCombination_String_Valid();
    testAddLinearCombination_Column_Valid();
    testAddLinearCombination_NullMatrixPointer();
    testAddLinearCombination_NullNumber();
    testAddLinearCombination_InvalidIndex();
    return 0;
}