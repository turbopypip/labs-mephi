#pragma once

#include <stdbool.h>
#include "MatrixElement.h"
#include "LoggingFlags.h"

// Definition of the pointer to matrix
typedef struct Matrix *pMatrix;

typedef struct Matrix {
    MatrixElement** elements;
    size_t size;
    size_t elementSize;
} Matrix;

/*
 * Create an empty matrix
 * ---------------------------------------------------------+
 * Returns:
 * - pointer to matrix
 * - NULL if there is no enough memory for allocation
 * - NULL if size is less or equal 0
 */
pMatrix createMatrix(size_t size, size_t elementSize);

/*
 * Delete a matrix by a pointer
 * ---------------------------------------------------------+
 * Returns:
 * - OK if success
 * - MATRIX_NULL if pMatrix is null
 */
int destroyMatrix(pMatrix Matrix);

/*
 * Print matrix
 * ---------------------------------------------------------+
 * Returns:
 * - OK if success
 * - MATRIX_NULL if pMatrix is null
 * - VALUE_NULL if element is null
 * - ELEMENTS_ARRAY_NULL if elements array is null
 */
int printMatrix(pMatrix);

/*
 * Add elements to the matrix
 * ---------------------------------------------------------+
 * returns:
 * - OK if success
 * - MATRIX_NULL if matrix is NULL
 * - FAILED_MAllOC if any malloc operation is failed
 * - ELEMENTS_ARRAY_NULL if any elements array is null
 * - FIELD_NULL if field pointer is null
 * - SIZE_ZERO if size is zero or less
 */
int addElements(pMatrix matrix, void*** elements, size_t size, pField pFiled);

/*
 * Swap element at [string][column] place
 * to another element
 * -------------------------------------------------------+
 * returns:
 * - OK if success
 * - INVALID_INDEX if there is no such index in matrix
 * - MATRIX_NULL if matrix is NULL
 * - ELEMENT_NULL if element is null
 */
int setElement(pMatrix matrix, size_t string,
               size_t column, MatrixElement element);

/*
 * Get an element at [string][column] place
 * -------------------------------------------------------+
 * returns:
 * - OK and element to *pElement
 * - MATRIX_NULL if matrix is NULL
 * - INVALID_INDEX if there is no index
 * [string][column] in matrix
 * - FAILED_MALLOC if any malloc operation is failed
 */
int getElement(pMatrix matrix, size_t string,
               size_t column, MatrixElement **pElement);

/*
 * Get matrix sum
 * -------------------------------------------------------+
 * returns:
 * - OK and result to *pMatrix if success
 * - MATRIX_NULL if any matrix is NULL
 * - INCOMPATIBLE_SIZE if matrix1, matrix2 and result
 * are incompatible by size
 * - ELEMENTS_ARRAY_NULL if any elements array is null
 * - VALUE_NULL if any value is null
 * - SUM_OVERFLOW if got overflow by sum
 */
int matrixSum(pMatrix matrix1, pMatrix matrix2, pMatrix result);

/*
 * Multiply a matrix by a number
 * -------------------------------------------------------+
 * returns:
 * - OK and result to *pMatrix if success
 * - MATRIX_NULL if result matrix is NULL
 * - ELEMENTS_ARRAY_NULL if any elements array is null
 * - ELEMENT_NULL if number is NULL
 * - MUL_OVERFLOW if got overflow by multiplication
 */
int multiplyByNumber(pMatrix matrix, void* number);

/*
 * Get a matrix multiplication
 * -------------------------------------------------------+
 * returns:
 * - OK and result to *pMatrix if success
 * - MATRIX_NULL if any matrix is NULL
 * - ELEMENTS_ARRAY_NULL if any elements array is null
 * - INCOMPATIBLE_SIZE if matrix1, matrix2 and result
 * are incompatible by size
 * - VALUE_NULL if any value is null
 * - FAILED_MAllOC if any malloc operation is failed
 * - SUM_OVERFLOW if got overflow by sum
 * - MUL_OVERFLOW if got overflow by multiplication
 */
int matrixMultiply(pMatrix matrix1, pMatrix matrix2, pMatrix result);

/*
 * Add a linear combination to the string or column in matrix
 * -------------------------------------------------------+
 * returns:
 * - OK if success
 * - MATRIX_NULL if matrix is NULL
 * - ELEMENT_NULL if number is NULL
 * - ELEMENTS_ARRAY_NULL if any elements array is null
 * - INVALID_INDEX if there is no such index in matrix
 */
int addLinearCombination(pMatrix matrix, size_t vectorIndex,
                         bool useStringOrColumn,
                         void* number, size_t resultVectorIndex);