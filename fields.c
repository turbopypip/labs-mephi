#include <limits.h>
#include <stdio.h>
#include <float.h>
#include <stdlib.h>
#include "Fields.h"
#include "LoggingFlags.h"
#include "ComplexNumber.h"

pField createField(void* zero,
                   int (*printValue)(void* element),
                   int (*sumValue)(void* a, void* b, void* result),
                   int (*subtractValue)(void* a, void* b, void* result),
                   int (*multiplyValue)(void* a, void* b, void* result),
                   int (*divideValue)(void* a, void* b, void* result),
                   int (*compareValue)(void* a, void* b)){
    if (zero == NULL ||
            printValue == NULL || sumValue == NULL ||
            subtractValue == NULL || multiplyValue == NULL ||
            divideValue == NULL ||compareValue == NULL) {
        return NULL;
    }

    pField field = (pField) malloc(sizeof(Field));
    if (field == NULL) return NULL;

    field->zero = zero;
    field->printValue = printValue;
    field->sumValue = sumValue;
    field->subtractValue = subtractValue;
    field->multiplyValue = multiplyValue;
    field->divideValue = divideValue;
    field->compareValue = compareValue;
    return field;
}

int destroyField(pField field) {
    if (field == NULL) {
        return FIELD_NULL;
    }
    free(field);
    return OK;
}

// Int operations
int printInt(void* value) {
    if (value == NULL) {
        return VALUE_NULL;
    }
    printf("%d", *((int*)value));
    return OK;
}

int sumInt(void* value1, void* value2, void* result) {
    if (value1 == NULL || value2 == NULL || result == NULL) {
        return VALUE_NULL;
    }
    int v1 = *((int*)value1);
    int v2 = *((int*)value2);

    if (((v2 > 0) && (v1 > INT_MAX - v2)) || ((v2 < 0) && (v1 < INT_MIN - v2))) {
        return SUM_OVERFLOW;
    } else {
        int res = v1 + v2;
        *((int*)result) = res;
        return OK;
    }
}

int multiplyInt(void* value1, void* value2, void* result) {
    if (value1 == NULL || value2 == NULL || result == NULL) {
        return VALUE_NULL;
    }
    int v1 = *((int*)value1);
    int v2 = *((int*)value2);

    if (v1 == 0 || v2 == 0) {
        *((int*)result) = 0;
        return OK;
    }
    if ((v1 > INT_MAX / v2) || (v1 < INT_MIN / v2)) {
        return MUL_OVERFLOW;
    } else {
        int res = v1 * v2;
        *((int*)result) = res;
        return OK;
    }
}

int subtractInt(void* a, void* b, void* result) {
    if (a == NULL || b == NULL || result == NULL) {
        return VALUE_NULL;
    }

    int v1 = *((int*)a);
    int v2 = *((int*)b);

    if (((v2 < 0) && (v1 > INT_MAX + v2)) || ((v2 > 0) && (v1 > INT_MIN + v2))) {
        return SUB_OVERFLOW;
    } else {
        int res = v1 - v2;
        *((int*)result) = res;
        return OK;
    }
}

int divideInt(void* a, void* b, void* c){
    if (a == NULL || b == NULL || c == NULL) {
        return VALUE_NULL;
    }

    if (*((int*)b) == 0){
        return DIV_BY_ZERO;
    }

    *(int*)c = *(int*)a / *(int*)b;
    return OK;
}

int compareInt(void* a, void* b){
    if (a == NULL || b == NULL) {
        return VALUE_NULL;
    }

    if (*(int*)a > *(int*)b){
        return FIRST_LARGER;
    } else if (*(int*)a < *(int*)b){
        return SECOND_LARGER;
    } else {
        return VALUES_EQUAL;
    }
}
// Int field
int intZero = 0;
Field IntField = {
        &intZero,
        &printInt,
        &sumInt,
        &subtractInt,
        &multiplyInt,
        &divideInt,
        &compareInt
};

// Float operations
int printFloat(void* value) {
    if (value == NULL) {
        return VALUE_NULL;
    }
    printf("%f", *((float*)value));
    return OK;
}

int sumFloat(void* value1, void* value2, void* result) {
    if (value1 == NULL || value2 == NULL || result == NULL) {
        return VALUE_NULL;
    }
    float v1 = *((float*)value1);
    float v2 = *((float*)value2);

    if (((v2 > 0) && (v1 > FLT_MAX - v2)) || ((v2 < 0) && (v1 < FLT_MIN - v2))) {
        return SUM_OVERFLOW;
    } else{
        float res = v1 + v2;
        *((float*)result) = res;
        return OK;
    }
}

int multiplyFloat(void* value1, void* value2, void* result) {
    if (value1 == NULL || value2 == NULL || result == NULL) {
        return VALUE_NULL;
    }
    float v1 = *((float*)value1);
    float v2 = *((float*)value2);

    if (v1 == 0.0 || v2 == 0.0) {
        *((float*)result) = 0;
        return OK;
    }
    if ((v1 > FLT_MAX / v2) || (v1 < FLT_MIN / v2)) {
        return MUL_OVERFLOW;
    } else {
        float res = v1 * v2;
        *((float*)result) = res;
        return OK;
    }
}

int subtractFloat(void* a, void* b, void* c){
    if (a == NULL || b == NULL || c == NULL) {
        return VALUE_NULL;
    }
    float v1 = *((float*)a);
    float v2 = *((float*)b);
    if (((v2 < 0) && (v1 > FLT_MAX + v2)) || ((v2 > 0) && (v1 > FLT_MIN + v2))) {
        return SUB_OVERFLOW;
    } else {
        float res = v1 - v2;
        *((float*)c) = res;
        return OK;
    }
}

int divideFloat(void* a, void* b, void* c){
    if (a == NULL || b == NULL || c == NULL) {
        return VALUE_NULL;
    }

    if (*((float*)b) == (float)0){
        return DIV_BY_ZERO;
    }
    *(float*)c = *(float*)a / *(float*)b;
    return OK;
}

int compareFloat(void* a, void* b){
    if (a == NULL || b == NULL) {
        return VALUE_NULL;
    }
    if (*(float*)a > *(float*)b){
        return FIRST_LARGER;
    } else if (*(float*)a < *(float*)b){
        return SECOND_LARGER;
    } else {
        return VALUES_EQUAL;
    }
}

// Float field
float floatZero = 0.0f;
Field FloatField = {
        &floatZero,
        &printFloat,
        &sumFloat,
        &subtractFloat,
        &multiplyFloat,
        &divideFloat,
        &compareFloat
};

// Double operations
int printDouble(void* value) {
    if (value == NULL) {
        return VALUE_NULL;
    }
    printf("%f", *((double*)value));
    return OK;
}

int sumDouble(void* value1, void* value2, void* result) {
    if (value1 == NULL || value2 == NULL || result == NULL) {
        return VALUE_NULL;
    }
    double v1 = *((double*)value1);
    double v2 = *((double*)value2);
    if (((v2 > 0) && (v1 > DBL_MAX - v2)) || ((v2 < 0) && (v1 < DBL_MIN - v2))){
        return SUM_OVERFLOW;
    } else {
        double res = v1 + v2;
        *((double*)result) = res;
        return OK;
    }
}

int multiplyDouble(void* value1, void* value2, void* result) {
    if (value1 == NULL || value2 == NULL || result == NULL) {
        return VALUE_NULL;
    }
    double v1 = *((double*)value1);
    double v2 = *((double*)value2);
    if (v1 == 0 || v2 == 0){
        *((double*)result) = 0;
        return OK;
    }
    if ((v1 > DBL_MAX / v2) || (v1 < DBL_MIN / v2)){
        return MUL_OVERFLOW;
    } else {
        double res = v1 * v2;
        *((double*)result) = res;
        return OK;
    }
}


int subtractDouble(void* a, void* b, void* c){
    if (a == NULL || b == NULL || c == NULL) {
        return VALUE_NULL;
    }
    double v1 = *((double*)a);
    double v2 = *((double*)b);
    if (((v2 < 0) && (v1 > DBL_MAX + v2)) || ((v2 > 0) && (v1 > DBL_MIN + v2))){
        return SUB_OVERFLOW;
    } else {
        double res = v1 - v2;
        *((double*)c) = res;
        return OK;
    }
}

int divideDouble(void* a, void* b, void* c){
    if (a == NULL || b == NULL || c == NULL) {
        return VALUE_NULL;
    }
    if (*(double*)b == 0){
        return DIV_BY_ZERO;
    }
    *(double*)c = *(double*)a / *(double*)b;
    return OK;
}

int compareDouble(void* a, void* b){
    if (a == NULL || b == NULL) {
        return VALUE_NULL;
    }
    if (*(double*)a > *(double*)b){
        return FIRST_LARGER;
    } else if (*(double*)a < *(double*)b){
        return SECOND_LARGER;
    } else {
        return VALUES_EQUAL;
    }
}

double doubleZero = 0.0;
Field DoubleField = {
        &doubleZero,
        &printDouble,
        &sumDouble,
        &subtractDouble,
        &multiplyDouble,
        &divideDouble,
        &compareDouble
};

// Long operations
int printLong(void* value) {
    if (value == NULL) {
        return VALUE_NULL;
    }
    printf("%ld", *((long*)value));
    return OK;
}

int sumLong(void* value1, void* value2, void* result) {
    if (value1 == NULL || value2 == NULL || result == NULL) {
        return VALUE_NULL;
    }
    long v1 = *((long*)value1);
    long v2 = *((long*)value2);
    if ((v2 > 0) && (v1 > LONG_MAX - v2) || (v2 < 0) && (v1 < LONG_MIN - v2)) {
        return SUM_OVERFLOW;
    } else {
        long res = v1 + v2;
        *((long*)result) = res;
        return OK;
    }
}

int multiplyLong(void* value1, void* value2, void* result) {
    if (value1 == NULL || value2 == NULL || result == NULL) {
        return VALUE_NULL;
    }
    long v1 = *((long*)value1);
    long v2 = *((long*)value2);
    if (v1 == 0 || v2 == 0){
        *((long*)result) = 0;
        return OK;
    }
    if ((v1 > LONG_MAX / v2) || (v1 < LONG_MIN / v2)){
        return MUL_OVERFLOW;
    } else {
        long res = v1 * v2;
        *((long*)result) = res;
        return OK;
    }
}

int subtractLong(void* a, void* b, void* c){
    if (a == NULL || b == NULL || c == NULL) {
        return VALUE_NULL;
    }
    long v1 = *((long*)a);
    long v2 = *((long*)b);
    if ((v2 < 0) && (v1 > LONG_MAX + v2) || (v2 > 0) && (v1 > LONG_MIN + v2)){
        return SUB_OVERFLOW;
    } else {
        long res = v1 - v2;
        *((long*)c) = res;
        return OK;
    }
}

int divideLong(void* a, void* b, void* c){
    if (a == NULL || b == NULL || c == NULL) {
        return VALUE_NULL;
    }
    if (*(long*)b == 0){
        return DIV_BY_ZERO;
    }
    *(long*)c = *(long*)a / *(long*)b;
    return OK;
}

int compareLong(void* a, void* b){
    if (*(long*)a > *(long*)b){
        return FIRST_LARGER;
    } else if (*(long*)a < *(long*)b){
        return SECOND_LARGER;
    } else {
        return VALUES_EQUAL;
    }
}

long longZero = 0l;
Field LongField = {
        &longZero,
        &printLong,
        &sumLong,
        &subtractLong,
        &multiplyLong,
        &divideLong,
        &compareLong
};

// Complex field
Complex intComplexZero = {&intZero, &intZero, sizeof(int), &IntField};
Field IntComplexField = {
        &intComplexZero,
        &printComplex,
        &sumComplex,
        &subtractComplex,
        &multiplyComplex,
        &divideComplex
};

Complex floatComplexZero = {&floatZero, &floatZero, sizeof(float), &FloatField};
Field FloatComplexField = {
        &floatComplexZero,
        &printComplex,
        &sumComplex,
        &subtractComplex,
        &multiplyComplex,
        &divideComplex
};

Complex doubleComplexZero = {&doubleZero, &doubleZero, sizeof(double), &DoubleField};
Field DoubleComplexField = {
        &doubleComplexZero,
        &printComplex,
        &sumComplex,
        &subtractComplex,
        &multiplyComplex,
        &divideComplex
};

Complex longComplexZero = {&longZero, &longZero, sizeof(long), &LongField};
Field LongComplexField = {
        &longComplexZero,
        &printComplex,
        &sumComplex,
        &subtractComplex,
        &multiplyComplex,
        &divideComplex
};