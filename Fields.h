#pragma once

#include <stdio.h>
#include "Fields.h"

// Definition of field
typedef struct Field {
    // pointer to zero
    void* zero;

    // -- Operations with values
    // Print value
    int (*printValue)(void* element);
    // Sum value
    int (*sumValue)(void* a, void* b, void* result);
    // Subtract value
    int (*subtractValue)(void* a, void* b, void* result);
    // Multiply value
    int (*multiplyValue)(void* a, void* b, void* result);
    // Divide value
    int (*divideValue)(void* a, void* b, void* result);
    // Compare value
    int (*compareValue)(void* a, void* b);
} Field;

typedef struct Field* pField;

pField createField(void* zero,
                   int (*printValue)(void* element),
                   int (*sumValue)(void* a, void* b, void* result),
                   int (*subtractValue)(void* a, void* b, void* result),
                   int (*multiplyValue)(void* a, void* b, void* result),
                   int (*divideValue)(void* a, void* b, void* result),
                   int (*compareValue)(void* a, void* b));

int destroyField(pField field);

// -- Basic fields
// Int field
Field IntField;
int printInt(void* value);
int multiplyInt(void* value1, void* value2, void* result);
int sumInt(void* value1, void* value2, void* result);
int subtractInt(void* value1, void* value2, void* result);
int divideInt(void* value1, void* value2, void* result);
int compareInt(void* value1, void* value2);

// Float field
Field FloatField;
int printFloat(void* value);
int multiplyFloat(void* value1, void* value2, void* result);
int sumFloat(void* value1, void* value2, void* result);
int subtractFloat(void* value1, void* value2, void* result);
int divideFloat(void* value1, void* value2, void* result);
int compareFloat(void* value1, void* value2);

// Double field
Field DoubleField;
int printDouble(void* value);
int multiplyDouble(void* value1, void* value2, void* result);
int sumDouble(void* value1, void* value2, void* result);
int subtractDouble(void* value1, void* value2, void* result);
int divideDouble(void* value1, void* value2, void* result);
int compareDouble(void* value1, void* value2);

// Long field
Field LongField;
int printLong(void* value);
int multiplyLong(void* value1, void* value2, void* result);
int sumLong(void* value1, void* value2, void* result);
int subtractLong(void* value1, void* value2, void* result);
int divideLong(void* value1, void* value2, void* result);
int compareLong(void* value1, void* value2);

//Complex fields
Field IntComplexField;
Field FloatComplexField;
Field DoubleComplexField;
Field LongComplexField;