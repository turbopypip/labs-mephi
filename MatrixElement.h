#include <stdio.h>
#include "Fields.h"

#pragma once

typedef struct MatrixElement{
    void* value;
    size_t size;
    Field* field;
} MatrixElement;

// Create MatrixElement
MatrixElement* createMatrixElement(void* value, size_t size, Field* field);