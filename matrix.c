#include <stdio.h>
#include <stdlib.h>
#include "Matrix.h"
#include "MatrixElement.h"
#include "LoggingFlags.h"
#include "Fields.h"

// Create an empty matrix
pMatrix createMatrix(size_t size, size_t elementSize){
    pMatrix matrix = (pMatrix)malloc(sizeof(Matrix));
    if (matrix == NULL){
        return NULL;
    } else if (size <= 0){
        free(matrix);
        return NULL;
    }

    matrix->elements = (MatrixElement**) calloc(size, sizeof(MatrixElement));
    if (matrix->elements == NULL){
        free(matrix);
        return NULL;
    }

    for (int i = 0; i < size; ++i) {
        matrix->elements[i] = (MatrixElement*) malloc(size * sizeof(MatrixElement));
        if (matrix->elements[i] == NULL){
            for (int j = 0; j <= i; ++j) {
                free(matrix->elements[j]);
            }
            free(matrix);
            return NULL;
        }
    }

    matrix->size = size;
    matrix->elementSize = elementSize;
    return matrix;
}

// Delete matrix
int destroyMatrix(pMatrix matrix) {
    if (matrix == NULL) return MATRIX_NULL;
    if (matrix->elements == NULL) {
        free(matrix);
        return OK;
    }

    for (int i = 0; i < matrix->size; ++i) {
        if (matrix->elements[i] != NULL){
            for (int j = 0; j < matrix->size; ++j) {
                if (matrix->elements[i][j].value != NULL) {
                    free(matrix->elements[i][j].value);
                }
            }
            free(matrix->elements[i]);
        }
    }

    free(matrix->elements);
    free(matrix);
    return OK;
}

// Add elements to matrix
int addElements(pMatrix matrix, void*** elements, size_t elementsSize, pField pField) {
    if (matrix == NULL){
        return MATRIX_NULL;
    } else if (elements == NULL){
        return ELEMENTS_ARRAY_NULL;
    } else if (pField == NULL){
        return FIELD_NULL;
    } else if (elementsSize == 0){
        return SIZE_ZERO;
    }

    MatrixElement** matrixElements = matrix->elements;
    if (matrixElements == NULL){
        matrixElements = (MatrixElement**) calloc(matrix->size, sizeof(MatrixElement));
        if (matrixElements == NULL){
            free(matrix);
            return FAILED_MALLOC;
        }

        for (int i = 0; i < matrix->size; ++i) {
            matrixElements[i] = (MatrixElement*) malloc(matrix->size * sizeof(MatrixElement));
            if (matrixElements[i] == NULL){
                for (int j = 0; j <= i; ++j) {
                    free(matrixElements[j]);
                }
                free(matrix);
                return FAILED_MALLOC;
            }
        }
    }

    for (int i = 0; i < matrix->size; ++i) {
        for (int j = 0; j < matrix->size; ++j) {
            matrixElements[i][j].field = pField;
            matrixElements[i][j].value = elements[i][j];
            matrixElements[i][j].size = elementsSize;
        }
    }
    matrix->elements = matrixElements;
    return OK;
}

// Print matrix
int printMatrix(pMatrix matrix){
    if (matrix == NULL) {
        return MATRIX_NULL;
    } else if (matrix->elements == NULL) {
        return ELEMENTS_ARRAY_NULL;
    }
    for (int i = 0; i < matrix->size; ++i) {
        for (int j = 0; j < matrix->size; ++j) {
            if (matrix->elements[i][j].value == NULL) return VALUE_NULL;
        }
    }
    for (int i = 0; i < matrix->size; ++i) {
        for (int j = 0; j < matrix->size; ++j) {
            matrix->elements[i][j].field->printValue(matrix->elements[i][j].value);
            printf(" ");

        }
        printf("\n");
    }
    return OK;
}


// Change element
int setElement(pMatrix matrix, size_t string, size_t column, MatrixElement element){
    if (matrix == NULL) return MATRIX_NULL;

    if (element.value == NULL) return ELEMENT_NULL;

    if (string >= matrix->size || column >= matrix->size) return INVALID_INDEX;

    matrix->elements[string][column] = element;
    return OK;
}

// Get element
int getElement(pMatrix matrix, size_t string, size_t column, MatrixElement **pElement){
    if (matrix == NULL) return MATRIX_NULL;

    if (string >= matrix->size || column >= matrix->size) return INVALID_INDEX;
    if (*pElement == NULL){
        *pElement = (MatrixElement*)malloc(sizeof(MatrixElement));
        if (*pElement == NULL) return FAILED_MALLOC;
    }
    **pElement = matrix->elements[string][column];
    return OK;
}

// Matrix sum
int matrixSum(pMatrix matrix1, pMatrix matrix2, pMatrix result){
    if (matrix1 == NULL || matrix2 == NULL || result == NULL) {
        return MATRIX_NULL;
    } else if (matrix1->size != matrix2->size || matrix1->size != result->size) {
        return INCOMPATIBLE_SIZE;
    } else if (matrix1->elements == NULL || matrix2->elements == NULL || result->elements == NULL) {
        return ELEMENTS_ARRAY_NULL;
    }

    for (int i = 0; i < matrix1->size; ++i) {
        for (int j = 0; j < matrix1->size; ++j) {
            if (matrix1->elements[i][j].value == NULL || matrix2->elements[i][j].value == NULL) {
                return VALUE_NULL;
            }
            if (matrix1->elements[i][j].field == NULL || matrix2->elements[i][j].field == NULL){
                return VALUE_NULL;
            }

            if (result->elements[i][j].value == NULL){
                result->elements[i][j].value = malloc(matrix1->elements[i][j].size);
                result->elements[i][j].field = matrix1->elements[i][j].field;
                result->elements[i][j].size = matrix1->elements[i][j].size;
                if (result->elements[i][j].value == NULL){
                    return FAILED_MALLOC;
                }
            }
            int log = matrix1->elements[i][j].field->sumValue(
                      matrix1->elements[i][j].value,
                      matrix2->elements[i][j].value,
                      result->elements[i][j].value);
            if (log != OK) return log;
        }
    }
    return OK;
}

// Multiply a matrix by number
int multiplyByNumber(pMatrix matrix, void* number) {
    if (matrix == NULL) {
        return MATRIX_NULL;
    } else if (number == NULL) {
        return ELEMENT_NULL;
    } else if (matrix->elements == NULL) {
        return ELEMENTS_ARRAY_NULL;
    }
    for (int i = 0; i < matrix->size; ++i) {
        for (int j = 0; j < matrix->size; ++j) {
            int log = matrix->elements[i][j].field->multiplyValue(
                      matrix->elements[i][j].value,
                      number,
                      matrix->elements[i][j].value);
            if (log != OK) return log;
        }
    }
    return OK;
}

// Multiply matrices
int matrixMultiply(pMatrix matrix1, pMatrix matrix2, pMatrix result){
    if (matrix1 == NULL || matrix2 == NULL || result == NULL) {
        return MATRIX_NULL;
    } else if (matrix1->size != matrix2->size || matrix1->size != result->size) {
        return INCOMPATIBLE_SIZE;
    } else if (matrix1->elements == NULL || matrix2->elements == NULL || result->elements == NULL) {
        return ELEMENTS_ARRAY_NULL;
    }

    for (int i = 0; i < matrix1->size; ++i) {
        for (int j = 0; j < matrix1->size; ++j) {
            for (int k = 0; k < result->size; ++k) {
                void* multipliedValue = malloc(matrix1->elementSize);
                if (multipliedValue == NULL){
                    return FAILED_MALLOC;
                }

                if (result->elements[i][j].value == NULL){
                    result->elements[i][j].value = malloc(matrix1->elements[i][j].size);
                    result->elements[i][j].field = matrix1->elements[i][j].field;
                    result->elements[i][j].size = matrix1->elements[i][j].size;
                    if (result->elements[i][j].value == NULL){
                        return FAILED_MALLOC;
                    }
                }

                int log = matrix1->elements[i][k].field->multiplyValue(
                        matrix1->elements[i][k].value,
                        matrix2->elements[k][j].value,
                        multipliedValue);
                if (log != OK) {
                    free(multipliedValue);
                    return log;
                }

                log = matrix1->elements[i][j].field->sumValue(
                        result->elements[i][j].value,
                        multipliedValue,
                        result->elements[i][j].value);
                if (log != OK) {
                    free(multipliedValue);
                    return log;
                }
            }
        }
    }
    return OK;
}

// Add linear combination
int addLinearCombination(pMatrix matrix, size_t vectorIndex,
                         bool useStringOrColumn,
                         void* number, size_t resultVectorIndex) {
    const int useColumn = 0;
    const int useString = 1;

    if (matrix == NULL) {
        return MATRIX_NULL;
    } else if (number == NULL) {
        return ELEMENT_NULL;
    } else if (matrix->elements == NULL) {
        return ELEMENTS_ARRAY_NULL;
    } else if (vectorIndex >= matrix->size ||
                resultVectorIndex >= matrix->size) {
        return INVALID_INDEX;
    }

    if (useStringOrColumn == useString) {
        for (int i = 0; i < matrix->size; ++i) {
            MatrixElement* sourceElement = &matrix->elements[vectorIndex][i];
            MatrixElement* targetElement = &matrix->elements[resultVectorIndex][i];

            void* temp = malloc(sizeof(number));
            if (temp == NULL) return FAILED_MALLOC;

            int log = sourceElement->field->multiplyValue(
                    sourceElement->value,
                    number,
                    temp
            );
            if (log != OK) {
                free(temp);
                return log;
            }

            log = targetElement->field->sumValue(
                    targetElement->value,
                    temp,
                    targetElement->value);
            if (log != OK) {
                free(temp);
                return log;
            }
        }

    } else {
        for (int i = 0; i < matrix->size; ++i) {
            MatrixElement* sourceElement = &matrix->elements[i][vectorIndex];
            MatrixElement* targetElement = &matrix->elements[i][resultVectorIndex];

            void* temp = malloc(sizeof(number));
            if (temp == NULL) return FAILED_MALLOC;

            int log = sourceElement->field->multiplyValue(
                    sourceElement->value,
                    number,
                    temp
            );
            if (log != OK) {
                free(temp);
                return log;
            }

            log = targetElement->field->sumValue(
                    targetElement->value,
                    temp,
                    targetElement->value);
            if (log != OK){
                free(temp);
                return log;
            }
        }
    }
    return OK;
}