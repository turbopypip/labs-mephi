#include <stdlib.h>
#include <stdio.h>
#include "ComplexNumber.h"
#include "LoggingFlags.h"


// Create complex number
pComplex createComplex(void* real, void* imaginary) {
    if (real == NULL || imaginary == NULL){
        return NULL;
    }
    pComplex complex = (pComplex) malloc(sizeof(Complex));
    if (complex == NULL){
        return NULL;
    }
    complex->real = real;
    complex->imaginary = imaginary;
    return complex;
}

// Delete complex number
int deleteComplex(pComplex complex){
    if (complex == NULL){
        return VALUE_NULL;
    }

    free(complex->real);
    free(complex->imaginary);
    free(complex);
    return OK;
}

// Print complex number
int printComplex(void* value){
    if (value == NULL){
        return VALUE_NULL;
    }

    pComplex complex = (pComplex) value;

    int log = complex->field->printValue(complex->real);
    if (log == VALUE_NULL) {
        return REAL_NULL;
    }

    if (complex->field->compareValue(complex->imaginary, complex->field->zero) == FIRST_LARGER){
        printf("+");
        log = complex->field->printValue(complex->imaginary);
        if (log == VALUE_NULL){
            return IMAGINARY_NULL;
        }
        printf("i");
        return OK;
    } else if (complex->field->compareValue(complex->imaginary, complex->field->zero) == SECOND_LARGER){
        return OK;
    } else {
        log = complex->field->printValue(complex->imaginary);
        if (log == VALUE_NULL){
            return IMAGINARY_NULL;
        }
        printf("i");
        return OK;
    }
}

// -- Operations

// Addition
int sumComplex(void* c1, void* c2, void* complexResult){
    if (c1 == NULL || c2 == NULL || complexResult == NULL){
        return VALUE_NULL;
    }
    pComplex a = (pComplex) c1;
    pComplex b = (pComplex) c2;
    pComplex result = (pComplex) complexResult;

    /*
     * Formula
     * (a + bi) + (c + di) = (a + c) + (b + d)i
     */

    // 1) a + c => real
    int log = a->field->sumValue(a->real, b->real, result->real);

    if (log == FIRST_NULL || log == SECOND_NULL || log == RESULT_NULL){
        return REAL_NULL;
    } else if (log == SUM_OVERFLOW){
        return SUM_OVERFLOW;
    }

    // 2) b + d => imaginary
    log = a->field->sumValue(a->imaginary, b->imaginary, result->imaginary);
    if (log == FIRST_NULL || log == SECOND_NULL || log == RESULT_NULL){
        return IMAGINARY_NULL;
    } else if (log == SUM_OVERFLOW){
        return SUM_OVERFLOW;
    }
    return OK;
}

// Subtraction
int subtractComplex(void* c1, void* c2, void* complexResult){
    if (c1 == NULL || c2 == NULL || complexResult == NULL){
        return VALUE_NULL;
    }
    pComplex a = (pComplex) c1;
    pComplex b = (pComplex) c2;
    pComplex result = (pComplex) complexResult;

    /*
     * Formula
     * (a + bi) - (c + di) = (a - c) + (b - d)i
     */

    // 1) a - c => real
    int log = a->field->subtractValue(a->real, b->real, result->real);

    if (log == FIRST_NULL || log == SECOND_NULL || log == RESULT_NULL){
        return REAL_NULL;
    } else if (log == SUB_OVERFLOW){
        return SUB_OVERFLOW;
    }

    // 2) b - d => imaginary
    log = a->field->subtractValue(a->imaginary, b->imaginary, result->imaginary);
    if (log == FIRST_NULL || log == SECOND_NULL || log == RESULT_NULL){
        return IMAGINARY_NULL;
    } else if (log == SUB_OVERFLOW){
        return SUB_OVERFLOW;
    }
    return OK;
}

// Multiplication by real number
int multiplyCompByScalar(void* c1, void* real, void* complexResult) {
    if (c1 == NULL || real == NULL || complexResult == NULL){
        return VALUE_NULL;
    }
    pComplex a = (pComplex) c1;
    pComplex result = (pComplex) complexResult;

    /*
     * Formula
     * (a + bi) * x = (a*x) + (b*x)i
     */

    // 1) a*x => real
    int log = a->field->multiplyValue(a->real, real, result->real);
    if (log == FIRST_NULL || log == SECOND_NULL || log == RESULT_NULL){
        return REAL_NULL;
    } else if (log == MUL_OVERFLOW){
        return MUL_OVERFLOW;
    }

    // 2) b*x => imaginary
    log = a->field->multiplyValue(a->imaginary, real, result->imaginary);
    if (log == FIRST_NULL || log == SECOND_NULL || log == RESULT_NULL){
        return IMAGINARY_NULL;
    } else if (log == MUL_OVERFLOW){
        return MUL_OVERFLOW;
    }
    return OK;
}

// Multiplication
int multiplyComplex(void* c1, void* c2, void* complexResult) {
    if (c1 == NULL || c2 == NULL || complexResult == NULL){
        return VALUE_NULL;
    }
    pComplex a = (pComplex) c1;
    pComplex b = (pComplex) c2;
    pComplex result = (pComplex) complexResult;

    /*
    * Formula
    * (a + bi) * (c + di) = (ac - bd) + (ad + bc)i
    */

    // Result variables
    void* real = malloc(result->valueSize);
    void* imaginary = malloc(result->valueSize);
    if (real == NULL || imaginary == NULL){
        return FAILED_MALLOC;
    }

    // Temporary variables
    void* temp = malloc(result->valueSize);
    if (temp == NULL){
        return FAILED_MALLOC;
    }

    // 1) a*c => real
    int log = a->field->multiplyValue(a->real, b->real, real);
    if (log == FIRST_NULL || log == SECOND_NULL || log == RESULT_NULL){
        return REAL_NULL;
    } else if (log == MUL_OVERFLOW){
        return MUL_OVERFLOW;
    }

    // 2) b*d => temp
    log = a->field->multiplyValue(a->imaginary, b->imaginary, temp);
    if (log == FIRST_NULL || log == SECOND_NULL || log == RESULT_NULL){
        return IMAGINARY_NULL;
    } else if (log == MUL_OVERFLOW) {
        return MUL_OVERFLOW;
    }

    // 3) ac - bd => real
    log = a->field->subtractValue(real, temp, real);
    if (log == SUB_OVERFLOW){
        return SUB_OVERFLOW;
    }

    // 4) a*d => imaginary
    log = a->field->multiplyValue(a->real, b->imaginary, imaginary);
    if (log == MUL_OVERFLOW){
        return MUL_OVERFLOW;
    }

    // 5) b*c => temp
    log = a->field->multiplyValue(a->imaginary, b->real, temp);
    if (log == MUL_OVERFLOW){
        return MUL_OVERFLOW;
    }

    // 6) ad + bc => imaginary
    log = a->field->sumValue(temp, imaginary, imaginary);
    if (log == SUM_OVERFLOW){
        return SUM_OVERFLOW;
    }

    result->real = real;
    result->imaginary = imaginary;
    free(temp);
    return OK;
}

// Division
int divideComplex(void* a, void* b, void* complexResult) {
    if (a == NULL || b == NULL || complexResult == NULL){
        return VALUE_NULL;
    }
    pComplex c1 = (pComplex) a;
    pComplex c2 = (pComplex) b;
    pComplex result = (pComplex) complexResult;

    /*
     * Formula
     *  a + bi     (ax + by) + (bx-ay)i
     * -------- = ----------------------
     *  x + yi         (x^2 + y^2)
    */

    // Result variables
    void *real = malloc(result->valueSize);
    void *imaginary = malloc(result->valueSize);
    if (real == NULL || imaginary == NULL){
        return FAILED_MALLOC;
    }

    // Temporary variables
    void *temp = malloc(result->valueSize);
    void *temp2 = malloc(result->valueSize);
    if (temp == NULL || temp2 == NULL){
        return FAILED_MALLOC;
    }

    // 1) a*x => real
    int log = c1->field->multiplyValue(c1->real, c2->real, real);
    if (log == FIRST_NULL || log == SECOND_NULL || log == RESULT_NULL){
        return REAL_NULL;
    } else if (log == MUL_OVERFLOW){
        return MUL_OVERFLOW;
    }

    // 2) b*y => temp
    log = c1->field->multiplyValue(c1->imaginary, c2->imaginary, temp);
    if (log == FIRST_NULL || log == SECOND_NULL || log == RESULT_NULL){
        return IMAGINARY_NULL;
    } else if (log == MUL_OVERFLOW){
        return MUL_OVERFLOW;
    }

    // 3) (ax + by) => real
    log = c1->field->sumValue(real, temp, real);
    if (log == SUM_OVERFLOW){
        return SUM_OVERFLOW;
    }

    // 4) x^2 => temp
    log = c1->field->multiplyValue(c1->real, c1->real, temp);
    if (log == MUL_OVERFLOW){
        return MUL_OVERFLOW;
    }

    // 5) y^2 => temp2
    log = c1->field->multiplyValue(c2->imaginary, c2->imaginary, temp2);
    if (log == MUL_OVERFLOW) {
        return MUL_OVERFLOW;
    }

    // 6) (x^2 + y^2) => temp2
    log = c1->field->sumValue(temp, temp2, temp2);
    if (log == SUM_OVERFLOW){
        return SUM_OVERFLOW;
    }

    /*
     *         ax + by
     *  7)  ------------- => real
     *        x^2 + y^2
     */
    log = c1->field->divideValue(real, temp2, real);
    if (log == DIV_OVERFLOW){
        return DIV_OVERFLOW;
    } else if (log == DIV_BY_ZERO){
        return DIV_BY_ZERO;
    }

    // 7) b*x => temp
    log = c1->field->multiplyValue(c1->imaginary, c2->real, temp);
    if (log == MUL_OVERFLOW){
        return MUL_OVERFLOW;
    }

    // 8) a*y => imaginary
    log = c1->field->multiplyValue(c1->real, c2->imaginary, imaginary);
    if (log == MUL_OVERFLOW){
        return MUL_OVERFLOW;
    }

    // 9) (bx-ay)i => imaginary
    log = c1->field->subtractValue(temp, imaginary, imaginary);
    if (log == SUB_OVERFLOW){
        return SUB_OVERFLOW;
    }

    /*
     *          bx-ay
     * 10)  ------------- i => imaginary
     *        x^2 + y^2
     */
    log = c1->field->divideValue(imaginary, temp2, imaginary);
    if (log == DIV_OVERFLOW){
        return DIV_OVERFLOW;
    } else if (log == DIV_BY_ZERO){
        return DIV_BY_ZERO;
    }

    result->real = real;
    result->imaginary = imaginary;

    free(temp);
    free(temp2);
    return 0;
}

// Division complex number by real number
int divideCompByScalar(void* c1, void* real, void* complexResult){
    if (c1 == NULL || real == NULL || complexResult == NULL){
        return VALUE_NULL;
    }
    pComplex a = (pComplex) c1;
    pComplex result = (pComplex) complexResult;

    /*
     * Formula
     * (a + bi)    a     b
     * -------- = --- + --- i
     *    x        x     x
     */

    /*
     *  1) a/x => real
     */
    int log = a->field->divideValue(real, a->real, result->real);
    if (log == FIRST_NULL || log == SECOND_NULL || log == RESULT_NULL){
        return REAL_NULL;
    } else if (log == DIV_BY_ZERO){
        return DIV_BY_ZERO;
    }

    /*
     * 2) b/x => imaginary
     */
    log = a->field->divideValue(real, a->imaginary, result->imaginary);
    if (log == FIRST_NULL || log == SECOND_NULL || log == RESULT_NULL){
        return IMAGINARY_NULL;
    } else if (log == DIV_BY_ZERO){
        return DIV_BY_ZERO;
    }
    return OK;
}