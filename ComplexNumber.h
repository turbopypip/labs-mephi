#pragma once

#include "Fields.h"

// Definition of complex number
typedef struct Complex {
    void* real;
    void* imaginary;
    size_t valueSize;
    Field *field;
} Complex;

typedef struct Complex* pComplex;

/*
 * Create complex number
 * ---------------------------------------------------------+
 * Returns:
 * - NULL if real of imaginary is NULL
 * - NULL if malloc fails
 * - pointer to new complex number
*/
pComplex createComplex(void* real, void* imaginary);

/*
 * Delete complex number
 * ---------------------------------------------------------+
 * Returns:
 * - OK if success
 * - VALUE_NULL if complex is NULL
*/
int deleteComplex(pComplex complex);

/*
 * Print complex number
 * ---------------------------------------------------------+
 * Returns:
 * - OK if success
 * - VALUE_NULL if complex is NULL
 * - REAL_NULL if real is NULL
 * - IMAGINARY_NULL if imaginary is NULL
*/
int printComplex(void* value);

// -- Operations

/*
 * Addition
 * ---------------------------------------------------------+
 * Returns:
 * - OK if success
 * - VALUE_NULL if c1 or c2 or complexResult is NULL
 * ---------------------------------------------------------+
 * Note:
 * Be sure that complex1, complex2 and complexResult are
 * pointers to Complex struct, and have same types of values
 */
int sumComplex(void* complex1, void* complex2, void* complexResult);

/*
 * Subtraction
 * ---------------------------------------------------------+
 * Returns:
 * - OK if success
 * - VALUE_NULL if c1 or c2 or complexResult is NULL
 * ---------------------------------------------------------+
 * Note:
 * Be sure that complex1, complex2 and complexResult are
 * pointers to Complex struct, and have same types of values
 */
int subtractComplex(void* c1, void* c2, void* complexResult);

/*
 * Multiplication between two complex numbers
 * ---------------------------------------------------------+
 * Returns:
 * - OK if success
 * - VALUE_NULL if c1 or c2 or complexResult is NULL
 * ---------------------------------------------------------+
 * Note:
 * Be sure that complex1, complex2 and complexResult are
 * pointers to Complex struct, and have same types of values
 */
int multiplyComplex(void* c1, void* c2, void* complexResult);

/*
 * Multiplication between complex number and scalar
 * ---------------------------------------------------------+
 * Returns:
 * - OK if success
 * - VALUE_NULL if c1 or real or complexResult is NULL
 * ---------------------------------------------------------+
 * Note:
 * Be sure that complex1, real and complexResult are
 * pointers to Complex struct, and have same types of values.
 * Be sure that scalar is same type with complexes' real and
 * imaginary values.
*/
int multiplyCompByScalar(void* c1, void* real, void* complexResult);

/*
 * Division
 * ---------------------------------------------------------+
 * Returns:
 * - OK if success
 * - VALUE_DIVISION_BY_ZERO if c2 == 0 + 0i
 * - VALUE_NULL if c1 or c2 or complexResult is NULL
 * ---------------------------------------------------------+
 * Note:
 * Be sure that complex1, complex2 and complexResult are
 * pointers to Complex struct, and have same types of values.
 */
int divideComplex(void* a, void* b, void* complexResult);

/*
 * Division complex number by scalar
 * ---------------------------------------------------------+
 * Returns:
 * - OK if success
 * - VALUE_DIVISION_BY_ZERO if scalar == 0
 * - VALUE_NULL if c1 or real or complexResult is NULL
 * ---------------------------------------------------------+
 * Note:
 * Be sure that complex1, real and complexResult are
 * pointers to Complex struct, and have same types of values.
 * Be sure that scalar is same type with complexes' real and
 * imaginary values.
 */
int divideCompByScalar(void* complex, void* real, void* result);