//--Flags

//-General
#define OK                     0
#define VALUE_NULL             1
#define ELEMENT_NULL           2
#define FAILED_MALLOC          3

//-Matrix flags
#define MATRIX_NULL            4
#define INVALID_INDEX          5
#define ELEMENTS_ARRAY_NULL    6
#define INCOMPATIBLE_SIZE      7
#define SIZE_ZERO              8

//-Complex flags
#define REAL_NULL              9
#define IMAGINARY_NULL         10


//--Binary arithmetic operations flags
//-Overflow
#define SUM_OVERFLOW           11
#define SUB_OVERFLOW           12
#define MUL_OVERFLOW           13
#define DIV_OVERFLOW           14

//-Division by zero flags
#define DIV_BY_ZERO            15

//-Incoming values are null
#define FIRST_NULL             16
#define SECOND_NULL            17
#define RESULT_NULL            18

//-Comparing flags
#define FIRST_LARGER           19
#define SECOND_LARGER          20
#define VALUES_EQUAL           21

// -- Field flags
#define FIELD_NULL             22